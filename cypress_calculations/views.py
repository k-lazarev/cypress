from django.shortcuts import render
from cypress_calculations.models import Customers
from cypress_calculations.models import collection_rate_mail, collection_rate_email, collection_rate_text, avg_time_to_collect_mail, avg_time_to_collect_text, avg_time_to_collect_email

def index(request):
  return render(request, 'cypress_calculations/index.html', {'obj': Customers.objects.all(), 'collection_rate_mail': collection_rate_mail, 'collection_rate_email': collection_rate_email, 'collection_rate_text': collection_rate_text, 'avg_time_to_collect_mail': avg_time_to_collect_mail, 'avg_time_to_collect_text': avg_time_to_collect_text, 'avg_time_to_collect_email': avg_time_to_collect_email})
