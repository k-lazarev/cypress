from django.apps import AppConfig


class CypressCalculationsConfig(AppConfig):
    name = 'cypress_calculations'
