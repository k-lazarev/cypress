# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.db.models import Avg, Count, Sum, Func, F

class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Customers(models.Model):
    billamount = models.FloatField(blank=True, null=True)
    billdate = models.DateField(blank=True, null=True)
    commstype = models.CharField(max_length=5, blank=True, null=True)
    paidamount = models.FloatField(blank=True, null=True)
    paiddate = models.DateField(blank=True, null=True)
    
    class Meta:
        managed = False
        db_table = 'customers'

# Collection rate by channel type ( = total amount billed / total amount paid)
# Avg. time to collect by channel type ( = time of payment - time bill sent)

amount_billed_mail = Customers.objects.filter(commstype='Mail').aggregate(Sum('billamount'))
amount_paid_mail = Customers.objects.filter(commstype='Mail').aggregate(Sum('paidamount'))

amount_billed_text = Customers.objects.filter(commstype='Text').aggregate(Sum('billamount'))
amount_paid_text = Customers.objects.filter(commstype='Text').aggregate(Sum('paidamount'))

amount_billed_email = Customers.objects.filter(commstype='Email').aggregate(Sum('billamount'))
amount_paid_email = Customers.objects.filter(commstype='Email').aggregate(Sum('paidamount'))

collection_rate_mail = amount_billed_mail.get('billamount__sum') / amount_paid_mail.get('paidamount__sum')
collection_rate_text = amount_billed_text.get('billamount__sum') / amount_paid_text.get('paidamount__sum')
collection_rate_email = amount_billed_email.get('billamount__sum') / amount_paid_email.get('paidamount__sum')

avg_time_to_collect_mail_delta = Customers.objects.filter(commstype='Mail').aggregate(average_difference=Avg(F('paiddate') - F('billdate')))
avg_time_to_collect_text_delta = Customers.objects.filter(commstype='Text').aggregate(average_difference=Avg(F('paiddate') - F('billdate')))
avg_time_to_collect_email_delta = Customers.objects.filter(commstype='Email').aggregate(average_difference=Avg(F('paiddate') - F('billdate')))

avg_time_to_collect_mail = avg_time_to_collect_mail_delta.get('average_difference')
avg_time_to_collect_text = avg_time_to_collect_text_delta.get('average_difference')
avg_time_to_collect_email = avg_time_to_collect_email_delta.get('average_difference')

class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
