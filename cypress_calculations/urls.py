from django.conf.urls import url
from . import views

app_name="cypress_calculations"

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
